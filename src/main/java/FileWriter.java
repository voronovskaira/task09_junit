import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;

public class FileWriter {
    public void writeToFile(String filePath, String text) {
        try {
            Path path = Paths.get(filePath);
            Files.write(path, Collections.singleton(text));

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    };
}
