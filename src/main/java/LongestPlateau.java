import java.util.*;
import java.util.stream.Collectors;

public class LongestPlateau {
    private Integer[] array;
    List<List<Integer>> listPlateau = new ArrayList<>();

    public LongestPlateau(Integer[] array) {
        this.array = array;
        this.listPlateau = parseOn();
    }

    public List<List<Integer>> parseOn() {
        if(array.length<=0){
            throw new IllegalArgumentException();
        }
        List<List<Integer>> listPlateau = new ArrayList<>();
        List<Integer> plateau = new ArrayList<>();
        plateau.add(array[0]);
        for (int i = 1; i < array.length; i++) {
            if (array[i] == array[i - 1]) {
                plateau.add(array[i]);
            }
            if (array[i] != array[i - 1]) {
                listPlateau.add(plateau);
                plateau = new ArrayList<>();
                plateau.add(array[i]);
            }
            if (i == array.length - 1) {
                listPlateau.add(plateau);
            }
        }
        List<List<Integer>> tmp = listPlateau.stream().filter(x -> x.size() > 1).collect(Collectors.toList());
        return tmp;
    }

    public List<Integer> findLongestPlateau() {
        Optional<List<Integer>> op = listPlateau.stream().sorted(new Comparator<List<Integer>>() {
            @Override
            public int compare(List<Integer> o1, List<Integer> o2) {
                return o2.size() - o1.size();
            }
        }).filter(x -> {
            int startIndex = Collections.indexOfSubList(Arrays.asList(array), x);
            int endIndex = startIndex + x.size();
            if (startIndex - 1 > 0 && endIndex < array.length && array[startIndex - 1] < x.get(0) && array[endIndex] < x.get(0))
                return true;
            else return false;
        }).findFirst();
        return op.get();

    }
}
