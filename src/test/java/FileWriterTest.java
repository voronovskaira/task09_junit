import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class FileWriterTest {
    private String FILE_PATH = "src/main/resources/file.txt";
    private String TEXT = "Test writing to file";
    private FileWriter fw = new FileWriter();

    @Test
    public void testWritingToFile() {
        fw.writeToFile(FILE_PATH, TEXT);
        assertTrue(readFileContent().equals(TEXT));
    }

    @AfterEach
    public void clearData() {
        Path path = Paths.get(FILE_PATH);
        try {
            Files.delete(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String readFileContent() {
        StringBuilder content = new StringBuilder();
        Path path = Paths.get(FILE_PATH);
        try {
            List<String> contents = Files.readAllLines(path);
            contents.forEach(content::append);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return content.toString();
    }

}
