import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class LongestPlateauTest {

    @Test
    public void testLongestPlateau() {
        Integer[] arr = {4, 2, 3, 3, 3, 2, 2, 1, 6, 9, 9, 1, 3, 3, 3, 1, 9, 9};
        LongestPlateau lp = new LongestPlateau(arr);
        List<Integer> tmp = lp.findLongestPlateau();
        assertEquals(tmp, Arrays.asList(3, 3, 3));
        assertTrue(tmp.equals(Arrays.asList(3, 3, 3)));
    }

    @Test
    public void testLongestPlateauEmpty() {
        Integer[] arr = {4, 2, 3, 3, 3, 2, 2, 1, 6, 9, 9, 1, 3, 3, 3, 1, 9, 9};
        LongestPlateau lp = new LongestPlateau(arr);
        List<Integer> tmp = lp.findLongestPlateau();
        assertFalse(tmp.equals(Arrays.asList("dhjds")));
    }

    @Test
    public void testSplittingPlateaus() {
        Integer[] arr = {4, 2, 3, 3, 3, 2, 2, 1, 6, 9, 9, 1, 3, 3, 3, 1};
        List<List<Integer>> expectedArray = new ArrayList<>();
        expectedArray.add(Arrays.asList(3, 3, 3));
        expectedArray.add(Arrays.asList(2, 2));
        expectedArray.add(Arrays.asList(9, 9));
        expectedArray.add(Arrays.asList(3, 3, 3));
        LongestPlateau lp = new LongestPlateau(arr);
        List<List<Integer>> tmp = lp.parseOn();
        assertEquals(tmp, expectedArray);
    }

}
